# README

In one tab:
`rails s`

If you don't start redis:
`brew services start redis`

In another terminal:
`bundle exec sidekiq`


This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
