class CreateClaims < ActiveRecord::Migration[6.1]
  def change
    create_table :claims do |t|
      t.references :boarding_pass, null: true, foreign_key: true

      t.timestamps
    end
  end
end
