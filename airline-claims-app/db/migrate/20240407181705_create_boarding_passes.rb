class CreateBoardingPasses < ActiveRecord::Migration[6.1]
  def change
    create_table :boarding_passes do |t|
      t.string :airline_code
      t.datetime :scheduled_departure_time
      t.datetime :scheduled_arrival_time
      t.string :origin_airport_code
      t.string :destination_airport_code
      t.string :passenger_name
      t.timestamps
    end
  end
end
