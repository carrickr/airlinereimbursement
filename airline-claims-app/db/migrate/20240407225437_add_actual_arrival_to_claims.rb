class AddActualArrivalToClaims < ActiveRecord::Migration[6.1]
  def change
    add_column :claims, :actual_arrived_at, :string
    add_column :claims, :actual_arrival_time, :datetime
  end
end
