class AddCategoryAndNotesColumnsToClaim < ActiveRecord::Migration[6.1]
  def change
    add_column :claims, :category, :string
    add_column :claims, :notes, :text
  end
end
