class AddJsonbToClaims < ActiveRecord::Migration[6.1]
  def change
    add_column :claims, :claim_decision, :jsonb
  end
end
