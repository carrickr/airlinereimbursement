class AddFlightNumberToBoardingPass < ActiveRecord::Migration[6.1]
  def change
    add_column :boarding_passes, :flight_number, :string
  end
end
