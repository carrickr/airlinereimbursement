class AddProcessingFlagToBoardingPass < ActiveRecord::Migration[6.1]
  def change
    add_column :boarding_passes, :processing, :boolean, default: true
  end
end
