class TestJob < ApplicationJob
  queue_as :default

  def perform(message:)
    # Do something later
    puts "#{message} #{Time.now}"
  end
end
