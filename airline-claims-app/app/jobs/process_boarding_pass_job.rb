require 'net/http'
require 'uri'

class ProcessBoardingPassJob < ApplicationJob
  queue_as :default

  FAKE_DATA = {
    'boarding-pass.jpg' => {
      airline_code: 'Delta',
      scheduled_departure_time: Time.parse('2019-01-18 6:00:00'),
      scheduled_arrival_time: Time.parse('2019-01-18 8:02:00'),
      origin_airport_code: 'ATL',
      destination_airport_code: 'LGA',
      passenger_name: 'Test Tester'
    },
    'muc-sfo.png' => {
      airline_code: 'Luftansa',
      scheduled_departure_time: Time.parse('2024-04-03 12:55:00'),
      scheduled_arrival_time: Time.parse('2024-04-3 22:40:00'),
      origin_airport_code: 'WAW',
      destination_airport_code: 'MUC',
      passenger_name: 'Witold Kowalcyzk'
    },
    'cdg-vie.jpg' => {
      airline_code: 'Austrian',
      flight_number: 'OS416',
      scheduled_departure_time: Time.parse('2023-10-23 15:10:00'),
      origin_airport_code: 'CDG',
      destination_airport_code: 'VIE',
      passenger_name: 'Mr Witold Kowalczyk'
    }
  }.freeze

  def perform(boarding_pass_id:)
    boarding_pass = BoardingPass.find(boarding_pass_id)

    return unless boarding_pass.processing?

    boarding_pass.update!(FAKE_DATA[boarding_pass.image.filename.to_s] || FAKE_DATA['boarding-pass.jpg'])

    # Call the ML Service
    uri = URI('http://localhost:3000/predict')

    request = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json')
    request.body = boarding_pass.to_ml_json.to_json

    response = Net::HTTP.start(uri.hostname, uri.port) do |http|
      http.request(request)
    end

    boarding_pass&.claim&.update(claim_decision: JSON.parse(response.body))

    # Save as a boarding pass
    boarding_pass.update!(
      processing: false,
      passenger_name: response['name'],
      airline_code: response['airline'],
      flight_number: response['flight_number'],
      scheduled_departure_time: response_departure(response),
      scheduled_arrival_time: response_arrival(response),
      origin_airport_code: response['city_of_departure'],
      destination_airport_code: response['city_of_arrival']
    )
  end

  private

  def response_departure(response)
    return unless response['scheduled_departure_datetime']

    Time.parse(response['scheduled_departure_datetime'])
  end

  def response_arrival(response)
    return unless response['scheduled_arrival_datetime']

    Time.parse(response['scheduled_arrival_datetime'])
  end
end
