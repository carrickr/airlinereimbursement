class Claim < ApplicationRecord
  belongs_to :boarding_pass, required: false

  CLAIM_CATEGORIES = {
    cancelled: 'Flight was cancelled',
    delayed: 'Flight was delayed',
    overbooked: 'Flight was overbooked',
    other: 'Other reason'
  }.freeze

  def time_delayed
    return unless actual_arrival_time && boarding_pass.scheduled_arrival_time

    actual_arrival_time - boarding_pass.scheduled_arrival_time
  end

  def eligible_delay?
    return false unless time_delayed

    time_delayed > 3.hours
  end

  def to_ml_string
    <<~ML_STRING
      #{claim_category_to_ml}
      #{actual_arrival_time_to_ml}
      #{actual_arrival_airport_to_ml}
      #{notes}
    ML_STRING
  end

  private

  def claim_category_to_ml
    return if category == :other

    "My #{Claim::CLAIM_CATEGORIES[category.to_sym]}"
  end

  def actual_arrival_time_to_ml
    return unless actual_arrival_time

    # TODO: Add time delayed & eleibility
    "I arrived at #{actual_arrival_time}. I was delayed by #{time_delayed}"
  end

  def actual_arrival_airport_to_ml
    return unless actual_arrived_at.present?

    "I actually arrived at #{actual_arrived_at} intead of my intended destination"
  end

  def is_eligible? 
    claim_data['is_eligible'].downcase == 'yes'
  end 

end
