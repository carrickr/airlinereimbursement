class BoardingPass < ApplicationRecord
  has_one_attached :image
  has_one :claim

  after_create_commit { ProcessBoardingPassJob.perform_later(boarding_pass_id: id) }

  def image_path
    ActiveStorage::Blob.service.path_for(image.key)
  end

  def to_ml_json
    {
      # image_path: image_path,
      passenger_name: passenger_name,
      airline_code: airline_code,
      flight_number: flight_number,
      scheduled_arrival_time: scheduled_arrival_time,
      scheduled_departure_time: scheduled_departure_time,
      origin_airport_code: origin_airport_code,
      destination_airport_code: destination_airport_code,
      user_info: claim&.to_ml_string
    }
  end
end
