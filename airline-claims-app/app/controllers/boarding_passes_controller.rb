class BoardingPassesController < ApplicationController
  def create
    boarding_pass = BoardingPass.new(boarding_pass_params)
    claim = Claim.new(boarding_pass:, **claim_params)
    if boarding_pass.save && claim.save
      redirect_to claim_path(id: claim.id)
    else
      redirect :back, alert: 'There was a problem saving your boarding pass'
    end
  end

  private

  def boarding_pass_params
    params.require(:boarding_pass).permit(:image)
  end

  def claim_params
    params.require(:claim).permit(:category, :notes, :actual_arrived_at, :actual_arrival_time)
  end
end
