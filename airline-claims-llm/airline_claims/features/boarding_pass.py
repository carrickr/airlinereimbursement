from airline_claims.prompts.base import boarding_pass_prompt, flight_eligibility
from airline_claims.llm.openai_client import get_vision_response, get_text_response

def scan_boarding_pass(image_path):
    return get_vision_response(image_path, boarding_pass_prompt)


def get_flight_eligibility(user_info, boarding_pass_details=None):
    prompt = flight_eligibility + f'boarding pass details: {boarding_pass_details}' + f'Information from user: {user_info}'
    return  get_text_response (prompt)