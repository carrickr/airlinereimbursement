from typing import Optional
from pydantic import BaseModel


class BoardingPassDetails(BaseModel):
    name: str
    airline: str
    flight_number: str
    date: str
    scheduled_arrival_time: str
    scheduled_departure_time: str
    city_of_departure: str
    city_of_arrival: str


class FailureOutputModel(BaseModel):
    error: str
    details: str


class FlightCompensationEligiblity(BaseModel):
    delay_time: str
    delay_reason: str
    delay_airport: str
    is_eligible: str
