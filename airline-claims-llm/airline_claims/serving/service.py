import io
import base64

from bentoml.io import JSON, File
import bentoml

from airline_claims.features.boarding_pass import scan_boarding_pass, get_flight_eligibility
from airline_claims.serving.models import BoardingPassDetails, FailureOutputModel


svc = bentoml.Service("airline_claims", runners=[])


@svc.api(
    input=JSON(),
    output=JSON(),
    route="/predict",
)
def boarding_pass_details(input, ctx: bentoml.Context):
    try:
        boarding_results = None
        if "image_path" in input:
            image_path = input["image_path"]
            boarding_results = scan_boarding_pass(image_path)
        else:
            boarding_results = input

        user_input = input["user_info"]

        results = get_flight_eligibility( user_info=user_input, boarding_pass_details = boarding_results)

        ctx.response.status_code = 200
        return results

    except Exception as e:
        failure_resp = FailureOutputModel(error="Internal Service Error", details=f"{e.args}")

        ctx.response.status_code = 500
        return failure_resp
