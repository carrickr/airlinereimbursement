boarding_pass_prompt = '''Given a boarding pass, extract the following information. If any of the fields doesn't exist, set a null value for that field.   

Return just a valid JSON and nothing more with the following fields:
- name: string
- airline: string 
- flight_number: string
- scheduled_arrival_datetime: string
- scheduled_departure_datetime: string
- city_of_departure: string 
- city_of_arrival: string
'''

flight_eligibility = '''
Using the boarding pass information and user information provided below, extract the information requested below. If any of the fields doesn't exist, set a null value for that field.   

Return only a valid JSON and nothing more with the following fields: 

- delay_time:
- delay_reason: string
- delay_airport: string
- is_eligible: string 
- reason: string (explain in detail why the claim is or isnt eligible by using 
                    STEP 1: check if flight itinerary makes it eligible for compensation (see the rule below) 
                        EU Regulation 261/2004 applies to:
                        Flights departing from an EU airport, regardless of the airline's nationality.
                        Flights arriving at an EU airport from outside the EU, provided the airline is based in an EU member state or in a country that is part of the European Economic Area (EEA) or Switzerland.
                        Flights from Iceland, Norway, and Switzerland, which are part of the EEA, are also covered by this regulation.
                    STEP 2: check if flight delay/cancellation makes it eligible for compensation (ie. +3 hours of delay to final destination - assessed by time of opened door)
                )

'''

