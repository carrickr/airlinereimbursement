import base64
import json
import openai

client = openai.OpenAI()


def get_text_response(prompt):
    messages=[
        {"role": "system", "content": prompt}
    ]
    response = client.chat.completions.create(
        model="gpt-4-32k", 
        max_tokens=300, 
        messages=messages, 
        temperature=0
    )
    
    content = response.choices[0].message.content
    json_str = content.lstrip("```json").rstrip("```")    
    print(json_str)
    return json_str


def encode_image(image_path):
    with open(image_path, "rb") as image_file:
        return base64.b64encode(image_file.read()).decode('utf-8')


def get_vision_response(image_path, prompt):    
    base64_image = encode_image(image_path)

    response = client.chat.completions.create(
    model="gpt-4-vision-preview",
    messages=[
        {
        "role": "user",
        "content": [
            {"type": "text", "text": prompt},
            {
            "type": "image_url",
            "image_url": {
                "url": f"data:image/jpeg;base64,{base64_image}",
            },
            },
        ],
        }
    ],
    max_tokens=300,
    temperature=0,
    )

    content = response.choices[0].message.content
    
    json_str = content.lstrip("```json").rstrip("```")    
    print(json_str)
    return json.loads(json_str)
