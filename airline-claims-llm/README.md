# stanford-law-hackathon-2024

This is Stanford Law Hackathon project. 


## Dev Setup 

- Install python 3.8 with `pyenv` or `brew`  
- Install poetry with `pip install poetry`
- Install task with `brew install go-task`

## ENV Vars

`cp env_template .env`

.env file with

```
OPENAI_API_KEY=
```

## Running the service

```
task serve:local 
```
